package com.globo.desafio.service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.globo.desafio.dto.DesafioResponseDto;
import com.globo.desafio.utils.Utils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DesafioServico {

	private static final String REGEX_ACENTOS = "[^\\p{ASCII}]";
	private static final String REGEX_PREPOSICOES = "(?i)(\\w)(\\s+)(e|do|da|do|das|de|di|du)(\\s+)(\\w)";
	private static final String REGEX_ALGARISMO_ROMANO = "^M{0,3}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$";

	@Qualifier("createEmailService")
	private ICreateService createEmailService;
	
	@Autowired
	private Utils utils;

	public DesafioResponseDto criaCombinacoes(String nome) throws IOException, URISyntaxException {
		return DesafioResponseDto.builder()
				.displayNames(criaEFormataDisplayNames(nome))
				.emailsDisponiveis(criaEFormataEmailsDisponiveis(nome))
				.loginsDisponiveis(criaEFormataLoginsDisponiveis(nome))
				.build();
	}

	private List<String> criaEFormataDisplayNames(String nome) {
		List<String> resultado = new ArrayList<>();
		
		List<String> criaEFormataDisplayNames = formataComposicaoDisplayName(nome);
		String primeiroNome = criaEFormataDisplayNames.get(0).trim();
		String ultimoNome = criaEFormataDisplayNames.get(criaEFormataDisplayNames.size() - 1).trim();
		String regex = "(?i)(e|do|da|do|das|de|di|du|dos)";

		resultado.add(primeiroNome + " " + ultimoNome);
		List<String> listaNomesDoMeio = criaEFormataDisplayNames.size() > 2 ?criaEFormataDisplayNames.subList(0, criaEFormataDisplayNames.size() - 1): new ArrayList<>();
		
		List<String> listCombinacao01 = new ArrayList<>();
		List<String> listCombinacao02 = new ArrayList<>();
		
		String formatoDados = null;
		for (int i = listaNomesDoMeio.size() - 1; i > 0; i--) {
			String currentName = listaNomesDoMeio.get(i);
			
			if (!currentName.matches(regex)) {
				resultado.add(primeiroNome + " " + currentName);
				listCombinacao01.add(primeiroNome + " " + currentName + " " + ultimoNome);
				listCombinacao02.add(primeiroNome + " " + currentName.substring(0,1) + ultimoNome.toLowerCase());
			} else {
				formatoDados = primeiroNome + " "+ currentName +" " + listaNomesDoMeio.get(i+1);
			}
			
		}
		if (formatoDados!= null)
			resultado.add(formatoDados);
		listCombinacao01.forEach(resultado::add);
		resultado.add(criaEFormataDisplayNames.stream().filter( e ->  !e.matches(regex)).collect(Collectors.joining(" ")) );
		listCombinacao02.forEach(resultado::add);
		resultado.add(criaEFormataDisplayNames.stream().collect(Collectors.joining(" ")) );
		resultado.add(criaEFormataDisplayNames.stream().collect(Collectors.joining(" ")) );

		return resultado;
	}

	private List<String> criaEFormataLoginsDisponiveis(String nome) {
		
		List<String> resultado = new ArrayList<>();
		
		if (nome.length() <= 3) {
			log.info("O nome precisa ter mais de 3 caracteres");
			return resultado;
		}
		
		List<String> listaNomesParaComporEmail = formataNomesParaComposicaoLoginEEmail(nome);
		if (listaNomesParaComporEmail.size()==1) {
			return mapMailList(listaNomesParaComporEmail);
		}
		String primeiroNome = listaNomesParaComporEmail.get(0).trim();
		String ultimoNome = listaNomesParaComporEmail.get(listaNomesParaComporEmail.size() - 1).trim();

		addLogin(resultado, primeiroNome);
		addLogin(resultado, ultimoNome);
		addLogin(resultado, primeiroNome.substring(0, 1).trim() + ultimoNome);

		// cria array de nomes do maio e listas de composicao para facilitar formatacao de email
		List<String> listaNomesDoMeio = listaNomesParaComporEmail.size() > 2 ?listaNomesParaComporEmail.subList(0, listaNomesParaComporEmail.size() - 1): new ArrayList<>();
		List<String> listComposicao01 = new ArrayList<>();
		List<String> listComposicao02 = new ArrayList<>();
		List<String> listComposicao03 = new ArrayList<>();

		for (int i = listaNomesDoMeio.size() - 1; i > 0; i--) {
			addLogin(resultado, primeiroNome.substring(0, 1).trim() + listaNomesDoMeio.get(i).trim());
		
			
			listComposicao01.add(primeiroNome + listaNomesDoMeio.get(i).substring(0,1));
			listComposicao02.add(primeiroNome.substring(0, 1).trim() + listaNomesDoMeio.get(i).trim());
			listComposicao03.add(primeiroNome + listaNomesDoMeio.get(i).substring(0, 1).trim());
		}

		addLogin(resultado, primeiroNome + ultimoNome.substring(0,1));
		listComposicao01.forEach( item -> addLogin(resultado, item));

		addLogin(resultado, subtr8posicoes(primeiroNome));
		addLogin(resultado, subtr8posicoes(ultimoNome));
		addLogin(resultado, subtr8posicoes(primeiroNome.substring(0,1) + ultimoNome));
		
		
		listComposicao02.forEach( item -> addLogin(resultado,subtr8posicoes( item)));
		addLogin(resultado, subtr8posicoes(primeiroNome + ultimoNome.substring(0,1)));
		listComposicao03.forEach( item -> addLogin(resultado, item));

		return resultado;
	}

	private String subtr8posicoes(String value) {
		if (value.length()>8) {
			return value.substring(0,8);
		}
		return value;
	}

	private void addLogin(List<String> resultado, String loginValue) {
		if (loginValue.trim().length()>=6 && loginValue.trim().length()<=8)
			resultado.add(loginValue.trim());  
		else
			log.info("Tamanho login inválido");
	}

	// FORMATACAO EMAIL
	private List<String> criaEFormataEmailsDisponiveis(final String nome) throws IOException, URISyntaxException {
		
		List<String> resultado = new ArrayList<>();
		
		if (nome.length() <= 3) {
			log.info("O nome precisa ter mais de 3 caracteres");
			return resultado;
		}
		
		List<String> listaNomesParaComporEmail = formataNomesParaComposicaoLoginEEmail(nome);
		if (listaNomesParaComporEmail.size()==1) {
			return mapMailList(listaNomesParaComporEmail);
		}

		String primeiroNome = listaNomesParaComporEmail.get(0).trim();
		String ultimoNome = listaNomesParaComporEmail.get(listaNomesParaComporEmail.size() - 1).trim();
		resultado.add(primeiroNome + "." + ultimoNome);

		// cria array de nomes do maio e listas de composicao para facilitar formatacao de email
		List<String> listaNomesDoMeio = listaNomesParaComporEmail.size() > 2 ?listaNomesParaComporEmail.subList(0, listaNomesParaComporEmail.size() - 1): new ArrayList<>();
		List<String> listComposicao01 = new ArrayList<>();
		List<String> listComposicao02 = new ArrayList<>();
		List<String> listComposicao03 = new ArrayList<>();

		for (int i = listaNomesDoMeio.size() - 1; i > 0; i--) {
			resultado.add(primeiroNome + "." + listaNomesDoMeio.get(i));
			listComposicao01.add(primeiroNome + "." + listaNomesDoMeio.get(i).substring(0, 1).trim() + ultimoNome );
			listComposicao02.add(primeiroNome.substring(0, 1).trim() + listaNomesDoMeio.get(i).trim());
			listComposicao03.add(primeiroNome.substring(0, 2).trim() + listaNomesDoMeio.get(i).trim());
		}
		
		resultado.addAll(listComposicao01);
		resultado.add(primeiroNome.substring(0, 1).trim() + ultimoNome);
		resultado.addAll(listComposicao02);
		resultado.add(primeiroNome.substring(0, 2).trim() + ultimoNome);
		resultado.addAll(listComposicao03);
		if (!listaNomesDoMeio.isEmpty()) {
			resultado.add(primeiroNome + "." + listaNomesDoMeio.get(listaNomesDoMeio.size() - 1) + "." + ultimoNome);
		}
		resultado.add(primeiroNome + ultimoNome.substring(0, 1).trim());
		resultado.add(primeiroNome);
		resultado.add(ultimoNome);

		return mapMailList(resultado);
	}

	private static List<String> mapMailList(List<String> listaNomesParaComporEmail) {
		return listaNomesParaComporEmail.stream().map(mail -> mail.toLowerCase().concat( "@teste.globo" )).collect(Collectors.toList());
	}

	private List<String> formataNomesParaComposicaoLoginEEmail(final String nome) {
		
		// remove acentos e preposicoes
		String nomeParaComposicao = Normalizer.normalize(nome, Normalizer.Form.NFD);
		String[] nomesParaComposicaoArray = nomeParaComposicao //
				.replaceAll(REGEX_ACENTOS, "") //
				.replaceAll(REGEX_PREPOSICOES, "$1 $5").split(" ");

		// busca lista de palavras proibidas
		List<String> listaBloqueio = utils.getListaBloqueio();

		// para cada palavra, remove algarismo romano e palavras ofensivas
		return Arrays.asList(nomesParaComposicaoArray).stream()
				.filter(algarismoRomano -> !algarismoRomano.matches(REGEX_ALGARISMO_ROMANO))
				.filter(ofensiveWord -> {
					Optional<String> findAny = listaBloqueio.stream().filter(test -> ofensiveWord.trim().toUpperCase().equals(test)).findAny();
					return !findAny.isPresent();
				}).map(String::toLowerCase).collect(Collectors.toList());
	}

	private List<String> formataComposicaoDisplayName(final String nome) {
		
		// remove acentos e preposicoes
		String nomeParaComposicao = Normalizer.normalize(nome, Normalizer.Form.NFD);
		String[] nomesParaComposicaoArray = nomeParaComposicao //
				.replaceAll(REGEX_ACENTOS, "").split(" ");

		// busca lista de palavras proibidas
		List<String> listaBloqueio = utils.getListaBloqueio();
		// para cada palavra, remove algarismo romano e palavras ofensivas
		return Arrays.asList(nomesParaComposicaoArray).stream()
				.filter(ofensiveWord -> {
					Optional<String> findAny = listaBloqueio.stream().filter(test -> ofensiveWord.trim().toUpperCase().equals(test)).findAny();
					return !findAny.isPresent();
				}).map(m -> m.substring(0,1).toUpperCase() + m.substring(1)).collect(Collectors.toList());
	}

}
